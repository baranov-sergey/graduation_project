﻿namespace DataProvider.Models
{
    public class StepEntity
    {
        public string? StepAction { get; set; }
        public string? Data { get; set; }
        public string? ExpectedResult { get; set; }
    }
}
