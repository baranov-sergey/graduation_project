﻿using DataProvider.Models;

namespace DataProvider.DataDistributor
{
    public class StepDataGenerator
    {
        public static StepEntity GetRandomStepEntity()
        {
            return new StepEntity()
            {
                StepAction = "Authorization",
                Data = "Set login and password",
                ExpectedResult = "Go to user profile"
            };
        }
    }
}
