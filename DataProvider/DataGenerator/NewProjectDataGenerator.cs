﻿using DataProvider.Models;

namespace DataProvider.DataDistributor
{
    public class NewProjectDataGenerator
    {
        public static NewProjectEntity GetRandomNewProjectEntity()
        {
            Random rnd = new Random();

            return new NewProjectEntity()
            {
                ProjectName = $"TestProject{rnd.Next(1000)}",
                ProjectCode = $"TP{rnd.Next(1000)}",
                Description = $"test_description-{rnd.Next()}"
            };
        }
    }
}
