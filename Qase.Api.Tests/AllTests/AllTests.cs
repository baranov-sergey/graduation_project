﻿using DataProvider.DataDistributor;
using DataProvider.Models;
using NUnit.Allure.Attributes;
using NUnit.Framework;
using Qase.Api.Tests.Client;
using Qase.Api.Tests.Client.Entities;

namespace Qase.Api.Tests.AllTests
{
    
    [TestFixture]
    internal class AllTests : BaseApiTest
    {
        private NewProjectEntity entityNewProject = NewProjectDataGenerator.GetRandomNewProjectEntity();

        [Category("Smoke")]
        [Test,Order(1)]
        public void CreateProjectTest()
        {
            ProjectRequest project = new()
            {
                title = entityNewProject.ProjectName,
                code = entityNewProject.ProjectCode
            };

            ProjectResponse result = QaseService.CreateProject(project);
            Assert.Multiple(() =>
            {
                Assert.That((int)BaseService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.status, Is.EqualTo(true));
            });

            entityNewProject.ProjectCode = result.result.code;
        }

        [Category("Smoke")]
        [Test, Order(2)]
        public void CreateCaseTest()
        {
            CaseRequest testCase = new()
            {
                title = "TestCase",
                severity = 3,
                priority = 3,
                description = "Test description"
            };

            CaseResponse result = QaseService.CreateCase(entityNewProject.ProjectCode, testCase);
            Assert.Multiple(() =>
            {
                Assert.That((int)BaseService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.status, Is.EqualTo(true));
            });
        }

        [Category("CriticalPath")]
        [Test, Order(3)]
        public void DeleteProjectTest()
        {
            ProjectResponse result = QaseService.DeleteProject(entityNewProject.ProjectCode);
            Assert.Multiple(() =>
            {
                Assert.That((int)BaseService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.status, Is.EqualTo(true));
            });
        }
    }
}
