using Core.Config;
using Core.Reporting;
using Core.Selenium;
using NUnit.Allure.Core;
using NUnit.Framework;
using Qase.Api.Tests.Client;

namespace Qase.Api.Tests.AllTests
{
    [AllureNUnit]
    [Parallelizable(ParallelScope.Fixtures)]
    [Category("API")]
    public class BaseApiTest
    {
        protected AllureReport report = new(Browser.Instance.Driver);

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            report.allure.CleanupResultDirectory();
            BaseService.Client = new(new Uri(ConfigProvider.Browser.BaseApiUrl));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            BaseService.Client.Dispose();
        }
    }
}
