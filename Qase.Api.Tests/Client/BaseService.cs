﻿using RestSharp;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Qase.Api.Tests.Client
{
    internal class BaseService
    {
        public static RestClient Client { get; set; }
        public static RestResponse Response { get; set; }
        public static RestRequest Request { get; set; }

        public static RestRequest SetRequest(string url, Method method)
        {
            return new RestRequest(url, method);
        }

        public static RestResponse SendRequest(string url, Method method)
        {
            Response = Client.ExecuteAsync(SetRequest(url, method)).Result;
            return Response;
        }

        public static RestResponse SendRequest(RestRequest request)
        {
            Response = Client.ExecuteAsync(request).Result;
            return Response;
        }

        public static JsonSerializerOptions GetJsonSerializerOptions()
        {
            return new JsonSerializerOptions()
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
            };
        }
    }
}
