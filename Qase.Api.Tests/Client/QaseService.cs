using Core.Config;
using Qase.Api.Tests.Client.Entities;
using RestSharp;
using System.Text.Json;

namespace Qase.Api.Tests.Client
{
    internal class QaseService : BaseService
    {
        public static ProjectResponse CreateProject(ProjectRequest project)
        {
            string json = JsonSerializer.Serialize(project, GetJsonSerializerOptions());
            var request = SetRequest("/v1/project", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);
            request.AddHeader("Token", ConfigProvider.Settings.Token);
            var response = SendRequest(request).Content;

            return JsonSerializer.Deserialize<ProjectResponse>(response);
        }

        public static ProjectResponse DeleteProject(string code)
        {
            var request = SetRequest($"/v1/project/{code}", Method.Delete);
            request.AddHeader("Token", ConfigProvider.Settings.Token);
            var response = SendRequest(request).Content;

            return JsonSerializer.Deserialize<ProjectResponse>(response);
        }

        public static CaseResponse CreateCase(string code, CaseRequest testCase)
        {
            string json = JsonSerializer.Serialize(testCase, GetJsonSerializerOptions());
            var request = SetRequest($"/v1/case/{code}", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(json);
            request.AddHeader("Token", ConfigProvider.Settings.Token);
            var response = SendRequest(request).Content;

            return JsonSerializer.Deserialize<CaseResponse>(response);
        }
    }
}
