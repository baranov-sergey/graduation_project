﻿namespace Qase.Api.Tests.Client.Entities
{
    public class ProjectRequest
    {
        public string title { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string access { get; set; }
        public string group { get; set; }
    }

    public class ProjectResponse
    {
        public bool status { get; set; }
        public Result result { get; set; }
    }

    public class Result
    {
        public string code { get; set; }
        public int id { get; set; }
    }
}
