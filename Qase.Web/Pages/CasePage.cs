﻿using Core.Utils.MyExtensions;
using DataProvider.DataDistributor;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.ObjectModel;

namespace Qase.Web.Pages
{
    public class CasePage : BasePage
    {
        #region Basic

        [FindsBy(How = How.Id, Using = "title")]
        public readonly IWebElement titleInput;

        [FindsBy(How = How.XPath, Using = "//label[text()='Status']")]
        public readonly IWebElement statusDropDown;

        [FindsBy(How = How.XPath, Using = "//p[@data-placeholder='For example: We can authorize on page http://example.com/login']")]
        public readonly IWebElement descriptionInput;

        [FindsBy(How = How.XPath, Using = "//label[text()='Suite']")]
        public readonly IWebElement suiteDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Severity']")]
        public readonly IWebElement severityDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Priority']")]
        public readonly IWebElement priorityDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Type']")]
        public readonly IWebElement typeDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Layer']")]
        public readonly IWebElement layerDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Is flaky']")]
        public readonly IWebElement isFlakyDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Milestone']")]
        public readonly IWebElement milestoneDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Behavior']")]
        public readonly IWebElement behaviorDropDown;

        [FindsBy(How = How.XPath, Using = "//label[text()='Automation status']")]
        public readonly IWebElement automationStatusDropDown;

        #endregion

        #region Parameters

        [FindsBy(How = How.XPath, Using = "//span[text()=' Add step']")]
        public readonly IWebElement addStepButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='OwrMko']")]
        public readonly IWebElement stepsContainer;
        private ReadOnlyCollection<IWebElement> steps => stepsContainer.FindElements(By.XPath(".//div[@class='OwrMko']"));

        #endregion

        [FindsBy(How = How.XPath, Using = "//button[@id='save-case']")]
        public readonly IWebElement saveButton;

        public CasePage()
        {
            PageFactory.InitElements(driver, this);
        }

        public CasePage SetTitle(string title)
        {
            titleInput.WaitClicableObjectThenClickAndSendKey(driver, title);
            return this;
        }

        public CasePage SelectElementInDropDown<T>(IWebElement webElement, T element)
        {
            webElement.WaitClickableObjectThenClick(driver);
            (webElement.FindElement(By.XPath($"//div[@class='vXlqZj NVBkcR']//div[text()='{element}']"))).WaitClickableObjectThenClick(driver);
            return this;
        }

        public CasePage AddStep()
        {
            addStepButton.WaitClickableObjectThenClick(driver);
            return this;
        }

        public void SetStepsAction()
        {
            var stepActionFields = stepsContainer.FindElements(By.XPath(".//div[@class='eKm5U9']/child::div[1]//p[@data-placeholder='Step Action']"));

            foreach (var field in stepActionFields)
            {
                field.WaitClicableObjectThenClickAndSendKey(driver, StepDataGenerator.GetRandomStepEntity().StepAction);
            }
        }

        public void SetStepsData()
        {
            var dataFields = stepsContainer.FindElements(By.XPath(".//div[@class='eKm5U9']/child::div[2]//p[@data-placeholder='Data']"));

            foreach (var field in dataFields)
            {
                action.MoveToElement(field)
                    .Pause(TimeSpan.FromMilliseconds(100))
                    .DoubleClick()
                    .SendKeys(StepDataGenerator.GetRandomStepEntity().Data)
                    .Perform();
            }
        }

        public void SetStepsExpectedResults()
        {
            var expectedResultsFields = stepsContainer.FindElements(By.XPath(".//div[@class='eKm5U9']/child::div[3]//p[@data-placeholder='Expected result']"));

            foreach (var field in expectedResultsFields)
            {
                action.MoveToElement(field)
                   .Pause(TimeSpan.FromMilliseconds(100))
                   .DoubleClick()
                   .SendKeys(StepDataGenerator.GetRandomStepEntity().ExpectedResult)
                   .Perform();
            }
        }

        public CasePage SetSteps()
        {
            SetStepsAction();
            SetStepsData();
            SetStepsExpectedResults();

            return this;
        }

        public ProjectPage SaveClick()
        {
            saveButton.ClickAndPageLoad(driver);
            return new ProjectPage();
        }
    }
}
