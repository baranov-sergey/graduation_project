﻿using Core.Utils.MyExtensions;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Qase.Web.Pages
{
    public class LoginPage : BasePage
    {
        [FindsBy(How = How.Name, Using = "email")]
        public IWebElement WorkEmailInput { get; private set; }

        [FindsBy(How = How.Name, Using = "password")]
        public IWebElement PasswordInput { get; private set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Remember me']")]
        public readonly IWebElement rememberMeCheckbox;

        [FindsBy(How = How.XPath, Using = "//span[text()='Sign in']")]
        public readonly IWebElement signInButton;

        public LoginPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public LoginPage SetWorkEmail(string email)
        {
            By locator = By.Name("email");
            WorkEmailInput = driver.RepeatFindElement(locator, 5);
            WorkEmailInput.SendKeys(email);
            return this;
        }

        public LoginPage SetPassword(string password)
        {
            By locator = By.Name("password");
            PasswordInput = driver.RepeatFindElement(locator, 5);
            PasswordInput.SendKeys(password);
            return this;
        }

        public LoginPage SetRememberMe()
        {
            rememberMeCheckbox.WaitClickableObjectThenClick(driver);
            return this;
        }

        public ProjectsPage ClickLogin()
        {
            signInButton.ClickAndPageLoad(driver);
            return new ProjectsPage();
        }
    }
}
