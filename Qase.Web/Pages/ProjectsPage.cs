﻿using Core.Utils.MyExtensions;
using OpenQA.Selenium;
using Qase.Web.UI.RadioButtons.NewProject;
using SeleniumExtras.PageObjects;

namespace Qase.Web.Pages
{
    public class ProjectsPage : BasePage
    {
        #region CreateNewProject

        [FindsBy(How = How.Id, Using = "createButton")]
        public readonly IWebElement createNewProjectButton;

        [FindsBy(How = How.Id, Using = "project-name")]
        public readonly IWebElement projectNameInput;

        [FindsBy(How = How.Id, Using = "project-code")]
        public readonly IWebElement projectCodeInput;

        [FindsBy(How = How.Name, Using = "description-area")]
        public readonly IWebElement descriptionInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='CC1vsd BZ2D59']")]
        public readonly IWebElement projectAccessTypeRadioButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='ySs0kb BZ2D59']")]
        public readonly IWebElement memberAccessRadioButton;

        [FindsBy(How = How.XPath, Using = ".//div[@class='pfDFL9']//span[text()='Cancel']")]
        public readonly IWebElement cancelButton;

        [FindsBy(How = How.CssSelector, Using = ".svg-inline--fa.fa-xmark")]
        public readonly IWebElement cancelIcon;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit']")]
        public readonly IWebElement createProjectButton;

        #endregion

        #region RemoveProject

        [FindsBy(How = How.XPath, Using = "//button[@class='G1dmaA eWFeX4 eij1r4']")]
        public readonly IWebElement meatballButton;

        [FindsBy(How = How.XPath, Using = "//button[@class='EehRY_ Wy99v3 fwhtHZ']")]
        public readonly IWebElement removeButton;

        [FindsBy(How = How.XPath, Using = "//button[@class='G1dmaA X8bxUI IAcAWv']")]
        public readonly IWebElement deleteProjectButton;

        #endregion

        public ProjectsPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public ProjectsPage ClickButton(IWebElement element)
        {
            element.WaitClickableObjectThenClick(driver);
            return this;
        }

        public ProjectsPage SetField(IWebElement element, string text)
        {
            element.WaitClicableObjectThenClickAndSendKey(driver, text);
            return this;
        }

        public ProjectsPage SetProjectAccessType(ProjectAccessTypeEnum type)
        {
            (projectAccessTypeRadioButton.FindElement(By.XPath($".//span[text()='{type}']"))).WaitClickableObjectThenClick(driver);
            return this;
        }

        public ProjectsPage SetMemberAccess(string memberAccess)
        {
            (memberAccessRadioButton.FindElement(By.XPath($".//span[text()='{memberAccess}']"))).WaitClickableObjectThenClick(driver);
            return this;
        }

        public ProjectPage CreateProject()
        {
            createProjectButton.ClickAndPageLoad(driver);
            return new ProjectPage();
        }
    }
}
