﻿using Core.Utils.MyExtensions;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Qase.Web.Pages
{
    public class StartPage : BasePage
    {
        [FindsBy(How = How.Id, Using = "signup")]
        public IWebElement StartForFreeButton { get; private set; }

        [FindsBy(How = How.XPath, Using = "//a[@id='signin' and text()='Login']")]
        public IWebElement LoginLink { get; private set; }

        public StartPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public SignUpPage GoToSignUp()
        {
            StartForFreeButton.ClickAndPageLoad(driver);
            return new SignUpPage();
        }

        public LoginPage GoToLogin()
        {
            By locator = By.XPath("//a[@id='signin' and text()='Login']");

            LoginLink = driver.RepeatFindElement(locator, 3);
            LoginLink.ClickAndPageLoad(driver);

            return new LoginPage();
        }
    }
}
