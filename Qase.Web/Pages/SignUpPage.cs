﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Qase.Web.Pages
{
    public class SignUpPage : BasePage
    {
        [FindsBy(How = How.Name, Using = "email")]
        public readonly IWebElement workEmailInput;

        [FindsBy(How = How.Name, Using = "password")]
        public readonly IWebElement passwordInput;

        [FindsBy(How = How.Name, Using = "passwordConfirmation")]
        public readonly IWebElement passwordConfirmationInput;

        [FindsBy(How = How.Name, Using = "emailPromoConfirmation")]
        public readonly IWebElement emailPromoConfirmationCheckbox;

        [FindsBy(How = How.XPath, Using = "//span[text()='Sign up with email']")]
        public readonly IWebElement signUpWithEmailButton;

        public SignUpPage()
        {
            PageFactory.InitElements(driver, this);
        }
    }
}
