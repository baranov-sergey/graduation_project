﻿using Core.Reporting;
using Core.Selenium;
using OpenQA.Selenium.Support.Events;

namespace Qase.Web.Pages
{
    public class BasePage : BaseInstances
    {
        public BasePage()
        {
            EventFiringWebDriver firingDriver = new(driver);

            firingDriver.ExceptionThrown += firingDriver_ExceptionThrown;
            firingDriver.Navigated += firingDriver_Navigated;
            firingDriver.ElementClicked += firingDriver_ElementClicked;
            firingDriver.FindElementCompleted += firingDriver_FindElementCompleted;

            driver = firingDriver;
        }

        static void firingDriver_ExceptionThrown(object sender, WebDriverExceptionEventArgs e)
        {
            AllureReport report = new(Browser.Instance.Driver);
            logger.Fatal($"from event handler - {e.ThrownException},\n{e.ThrownException.StackTrace}");
            report.GetErrorScreenshot();
        }

        static void firingDriver_Navigated(object sender, WebDriverNavigationEventArgs e)
        {
            logger.Information($"from event handler - Navigated - {e.Driver}");
        }

        static void firingDriver_ElementClicked(object sender, WebElementEventArgs e)
        {
            logger.Information($"from event handler - ElementClicked - {e.Element}");
        }

        static void firingDriver_FindElementCompleted(object sender, FindElementEventArgs e)
        {
            logger.Information($"from event handler - FindElementCompleted - {e.FindMethod}");
        }
    }
}
