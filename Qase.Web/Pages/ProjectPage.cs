﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using Core.Utils.MyExtensions;

namespace Qase.Web.Pages
{
    public class ProjectPage : BasePage
    {

        [FindsBy(How = How.Id, Using = "create-case-button")]
        public readonly IWebElement caseButton;

        public ProjectPage()
        {
            PageFactory.InitElements(driver, this);
        }

        public ProjectPage CreateCase()
        {
            caseButton.ClickAndPageLoad(driver);
            return this;
        }
    }
}
