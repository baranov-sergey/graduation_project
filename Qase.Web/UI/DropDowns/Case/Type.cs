﻿namespace Qase.Web.UI.DropDowns.Case
{
    public enum Type
    {
        Other,
        Functional,
        Smoke,
        Regression,
        Security,
        Usability,
        Performance,
        Acceptance,
        Compatibility,
        Integration,
        Exploratory
    }
}
