# Описание API и UI тестов для сервиса Qase

## Технологии
- .NET 8.0
- NUnit
- Configuration
- Serilog
- Selenium
- RestSharp

## Модули
- `Core`
	- Config (Классы для чтения данных из конфигурационного файла)
	- Enums (Вспомогательные перечисления)
	- Reporting (Классы для отчета)
	- Selenium (Создание веб-драйвера и операции над ним)
	- Utils (Утилиты, включает класс логирования) 
- `DataProvider`
	- DataGenerator (Генератор данных для тестов, шагов и т.д.)
	- Models (Модели для генератора данных)
- `Qase.Api.Tests`
	- AllTests (Все API-тесты)
	- Client (Классы клиента для работы с запросами и ответами)
- `Qase.Ui.Tests`
	- AllTests (Все UI-тесты)
	- Steps (Классы с наборами шагов)
- `Qase.Web`
	- Pages (Структуры страниц)
	- UI (Элементы страницы)
	
## Как запускать все тесты
```
 dotnet test
```

## Как запускать UI-тесты
```
 dotnet test --filter "Category=UI"
```

## Как запускать API-тесты 
```
 dotnet test --filter "Category=API"
```

## Составление отчета
Чтобы создать отчёт, запустите CLI и перейдите в папку `GraduationProject\Qase.Api.Tests\bin\Debug\net8.0` или
`GraduationProject\Qase.Api.Tests\bin\Debug\net8.0`, введите команду:
```
allure serve allure-results
```

## Логирование
Уровень логов установлен information, включает в себя следующие состояния логировния: Fatal, Error, Warning, Information.
Запись логирования ведется в консоль и файл(путь `GraduationProject\Qase.Api.Tests\bin\Debug\net8.0` или `GraduationProject\Qase.Api.Tests\bin\Debug\net8.0`).