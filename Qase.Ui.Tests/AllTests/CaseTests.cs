﻿using Qase.Ui.Tests.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture, Order(4)]
    internal class CaseTests : BaseUiTest
    {
        private CaseSteps caseSteps;

        [SetUp]
        public void SetUp()
        {
            caseSteps = new();
        }


        [Category("Smoke")]
        [Test]
        public void CreateCaseTest()
        {
            caseSteps.CreateCase();
            Assert.IsTrue(caseSteps.casePage.saveButton.Displayed);
            caseSteps.SaveCase();
        }


        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
