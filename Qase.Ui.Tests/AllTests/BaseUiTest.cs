﻿global using NUnit.Framework;
using Core.Reporting;
using Core.Selenium;
using NUnit.Allure.Core;

namespace Qase.Ui.Tests.AllTests
{
    [AllureNUnit]
    [Parallelizable(ParallelScope.Fixtures)]
    [Category("UI")]
    public class BaseUiTest : BaseInstances
    {
        protected AllureReport report = new(Browser.Instance.Driver);

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            report.allure.CleanupResultDirectory();
            browser.Open(baseUrl);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            browser.Quit();
        }
    }
}
