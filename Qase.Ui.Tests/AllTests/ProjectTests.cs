﻿using Qase.Ui.Tests.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture, Order(3)]
    internal class ProjectTests : BaseUiTest
    {
        private ProjectSteps projectSteps;

        [SetUp]
        public void SetUp()
        {
            projectSteps = new();
        }

        [Category("Smoke")]
        [Test]
        public void AddCaseTest()
        {
            projectSteps.AddCase();
        }

        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
