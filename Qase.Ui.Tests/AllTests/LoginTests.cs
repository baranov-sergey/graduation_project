﻿using Qase.Ui.Tests.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture, Order(1)]
    internal class LoginTests : BaseUiTest
    {
        private LoginSteps loginSteps;

        [SetUp]
        public void SetUp()
        {
            loginSteps = new();
        }

        [Category("Smoke")]
        [Test]
        public void LoginTest()
        {
            loginSteps.Login();
            Assert.IsTrue(loginSteps.loginPage.rememberMeCheckbox.Enabled);
            loginSteps.GoToWorkspace();
            Assert.IsTrue(browser.Driver.Url.Contains("https://app.qase.io/projects"));
        }

        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
