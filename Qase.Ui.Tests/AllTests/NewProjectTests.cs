﻿using Qase.Ui.Tests.Steps;

namespace Qase.Ui.Tests.AllTests
{
    [TestFixture, Order(2)]
    internal class NewProjectTests : BaseUiTest
    {
        private ProjectsSteps newProjectSteps;

        [SetUp]
        public void SetUp()
        {
            newProjectSteps = new();
        }

        [Category("Smoke")]
        [Test]
        public void NewProjectTest()
        {
            newProjectSteps.AddNewPublicProject();
            Assert.IsTrue(newProjectSteps.projectsPage.createProjectButton.Displayed);
            newProjectSteps.CreateProject();
        }

        [TearDown]
        public void TearDown()
        {
            report.GetErrorScreenshot();
        }
    }
}
