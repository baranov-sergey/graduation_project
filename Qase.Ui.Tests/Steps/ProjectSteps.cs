﻿using Qase.Ui.Tests.AllTests;
using Qase.Web.Pages;

namespace Qase.Ui.Tests.Steps
{
    public class ProjectSteps : BaseUiTest
    {
        private readonly ProjectsSteps newProjectSteps = new();
        public readonly ProjectPage projectPage = new();

        public ProjectSteps()
        {
            newProjectSteps.AddNewPublicProject();
            newProjectSteps.CreateProject();
        }

        public void AddCase()
        {
            projectPage.CreateCase();
        }
    }
}
