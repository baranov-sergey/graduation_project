﻿using OpenQA.Selenium;
using Qase.Ui.Tests.AllTests;
using Qase.Web.Pages;
using SeleniumExtras.WaitHelpers;

namespace Qase.Ui.Tests.Steps
{
    public class LoginSteps : BaseUiTest
    {
        public readonly StartPage startPage = new();
        public readonly LoginPage loginPage = new();

        public LoginSteps() { }

        public void Login()
        {
            startPage.GoToLogin();
            loginPage.SetWorkEmail(workEmail)
                .SetPassword(password)
                .SetRememberMe();
        }

        public void GoToWorkspace()
        {
            loginPage.ClickLogin();
            wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#layout")));
        }
    }
}
