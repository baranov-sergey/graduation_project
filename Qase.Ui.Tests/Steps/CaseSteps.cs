﻿using Qase.Ui.Tests.AllTests;
using Qase.Web.Pages;
using Qase.Web.UI.DropDowns.Case;

namespace Qase.Ui.Tests.Steps
{
    public class CaseSteps : BaseUiTest
    {
        private readonly ProjectSteps projectSteps = new();
        public readonly CasePage casePage = new();

        public CaseSteps()
        {
            projectSteps.AddCase();
        }

        public void CreateCase()
        {
            Random rnd = new Random();

            casePage.SetTitle($"Test{rnd.Next()}")
                .SelectElementInDropDown(casePage.statusDropDown, Status.Draft)
                .SelectElementInDropDown(casePage.severityDown, Severity.Critical)
                .SelectElementInDropDown(casePage.priorityDown, Priority.Medium)
                .SelectElementInDropDown(casePage.typeDropDown, Web.UI.DropDowns.Case.Type.Smoke)
                .SelectElementInDropDown(casePage.layerDropDown, Layer.E2E)
                .SelectElementInDropDown(casePage.behaviorDropDown, Behavior.Positive);

            casePage.AddStep()
                .AddStep()
                .AddStep()
                .SetSteps();
        }

        public void SaveCase()
        {
            casePage.SaveClick();
        }
    }
}
