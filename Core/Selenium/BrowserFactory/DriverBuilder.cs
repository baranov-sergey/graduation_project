﻿using OpenQA.Selenium;

namespace Core.Selenium.BrowserFatory
{
    public abstract class DriverBuilder
    {
        public abstract IWebDriver GetDriver();
    }
}
