﻿using Core.Config;
using Core.Enums;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;

namespace Core.Selenium.BrowserFatory
{
    public class EdgeDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            EdgeOptions edgeOptions = new();

            if (ConfigProvider.Browser.HeadLess)
            {
                edgeOptions.AddArgument("--headless");
                edgeOptions.AddArgument("--disable-gpu");
            }

            //chromeOptions.AddArguments("start-maximized");
            edgeOptions.AddArguments("--window-size=1920,1080");

            if (ConfigProvider.Browser.ManagePlace == ManagePlace.local)
            {
                EdgeDriver edgeDriver = new(edgeOptions);
                return edgeDriver;
            }

            string remoteUrl = Environment.GetEnvironmentVariable("SELENIUM_REMOTE_URL") ?? "http://localhost:4444";
            return new RemoteWebDriver(new Uri(remoteUrl), edgeOptions);
        }
    }
}
