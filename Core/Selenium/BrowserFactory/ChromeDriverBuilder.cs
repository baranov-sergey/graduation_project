﻿using Core.Config;
using Core.Enums;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace Core.Selenium.BrowserFatory
{
    public class ChromeDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            ChromeOptions chromeOptions = new();

            if (ConfigProvider.Browser.HeadLess)
            {
                chromeOptions.AddArgument("--headless");
                chromeOptions.AddArgument("--disable-gpu");
            }

            //chromeOptions.AddArguments("start-maximized");
            chromeOptions.AddArguments("--window-size=1920,1080");

            if (ConfigProvider.Browser.ManagePlace == ManagePlace.local)
            {
                ChromeDriver chromeDriver = new(chromeOptions);
                return chromeDriver;
            }

            string remoteUrl = Environment.GetEnvironmentVariable("SELENIUM_REMOTE_URL") ?? "http://localhost:4444";
            return new RemoteWebDriver(new Uri(remoteUrl), chromeOptions);
        }
    }
}
