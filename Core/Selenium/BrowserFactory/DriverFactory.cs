﻿using Core.Enums;
using OpenQA.Selenium;

namespace Core.Selenium.BrowserFatory
{
    public static class DriverFactory
    {
        public static IWebDriver CreateBrowser(BrowserType name)
        {
            return name switch
            {
                BrowserType.Chrome => new ChromeDriverBuilder().GetDriver(),
                BrowserType.FireFox => new FireFoxDriverBuilder().GetDriver(),
                BrowserType.Edge => new EdgeDriverBuilder().GetDriver(),
                _ => throw new ArgumentOutOfRangeException(name.ToString(), $"No such browser {name}")
            };
        }
    }
}
