﻿using Core.Config;
using Core.Enums;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace Core.Selenium.BrowserFatory
{
    public class FireFoxDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            FirefoxOptions fireFoxOptions = new();

            if (ConfigProvider.Browser.HeadLess)
            {
                fireFoxOptions.AddArgument("--headless");
                fireFoxOptions.AddArgument("--disable-gpu");
            }

            //chromeOptions.AddArguments("start-maximized");
            fireFoxOptions.AddArguments("--window-size=1920,1080");

            if (ConfigProvider.Browser.ManagePlace == ManagePlace.local)
            {
                FirefoxDriver fireFoxDriver = new(fireFoxOptions);
                return fireFoxDriver;
            }

            string remoteUrl = Environment.GetEnvironmentVariable("SELENIUM_REMOTE_URL") ?? "http://localhost:4444";
            return new RemoteWebDriver(new Uri(remoteUrl), fireFoxOptions);
        }
    }
}
