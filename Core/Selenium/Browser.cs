﻿using Core.Enums;
using Core.Selenium.BrowserFatory;
using OpenQA.Selenium;

namespace Core.Selenium
{
    public class Browser
    {
        private static readonly ThreadLocal<Browser> browserInstances = new();
        public IWebDriver Driver { get; private set; }
        public static Browser? Instance => browserInstances.Value ?? (browserInstances.Value = new Browser());

        private Browser()
        {
            Driver = DriverFactory.CreateBrowser(BrowserType.Chrome);
        }

        public void Open(string url)
        {
            TimeSpan span = TimeSpan.FromSeconds(3);

            try
            {
                Driver.Manage().Timeouts().PageLoad = span;
                Driver.Url = url;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine($"{DateTime.Now} Page load takes more than {span.TotalSeconds} seconds," +
                    $"\nWebDriverTimeoutException: {e.Message}");
            }

            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
        }

        public void Close()
        {
            Driver?.Close();
        }

        public void Quit()
        {
            Driver?.Quit();
            Driver?.Dispose();
            browserInstances.Value = null;
            Driver = null;
        }
    }
}
