﻿using Core.Config;
using Core.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Serilog;

namespace Core.Selenium
{
    public class BaseInstances
    {
        protected Browser browser = Browser.Instance;
        protected IWebDriver driver = Browser.Instance.Driver;
        protected WebDriverWait wait = new(Browser.Instance.Driver, TimeSpan.FromSeconds(3));
        protected Actions action = new(Browser.Instance.Driver);
        protected static ILogger logger = Logger.Instance;

        protected readonly string baseUrl = ConfigProvider.Browser.BaseUrl;
        protected readonly string workEmail = ConfigProvider.Settings.Email;
        protected readonly string password = ConfigProvider.Settings.Password;
    }
}
