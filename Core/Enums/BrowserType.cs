﻿namespace Core.Enums
{
    public enum BrowserType
    {
        Chrome,
        Edge,
        Yandex,
        FireFox,
    }
}
