﻿using Core.Enums;

namespace Core.Config.ModelsConfig
{
    public class BrowserConfig
    {
        public string BaseUrl { get; set; }
        public string BaseApiUrl { get; set; }
        public ManagePlace ManagePlace { get; set; }
        public bool HeadLess { get; set; }
        public BrowserType Type { get; set; }
    }
}
