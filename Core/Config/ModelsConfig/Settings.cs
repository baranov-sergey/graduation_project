﻿namespace Core.Config.ModelsConfig
{
    public class Settings
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
