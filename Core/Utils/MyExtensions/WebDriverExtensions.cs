﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Serilog;

namespace Core.Utils.MyExtensions
{
    public static class WebDriverExtensions
    {
        private static object locker = new();
        private static ILogger _log = Logger.Instance;

        public static IWebElement FindElement(this IWebDriver driver, By locator, int timeoutInSeconds = 0)
        {
            lock (locker)
            {
                if (timeoutInSeconds > 0)
                {
                    WebDriverWait wait = new(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                    return wait.Until(drv => drv.FindElement(locator));
                }

                return driver.FindElement(locator);
            }
        }

        public static IWebElement RepeatFindElement(this IWebDriver driver, By locator, int timeoutInSeconds = 0, int replays = 5)
        {
            lock (locker)
            {
                IWebElement element = null;

                for (int i = 0; i < replays; i++)
                {
                    try
                    {
                        element = driver.FindElement(locator, timeoutInSeconds);
                        break;
                    }
                    catch (NoSuchElementException e)
                    {
                        _log.Warning(e, $"WebElement not found, repeat find element");
                        continue;
                    }

                    catch (WebDriverTimeoutException e)
                    {
                        _log.Warning(e, $"WebElement not found, repeat find element");
                        continue;
                    }
                }

                return element;
            }
        }
    }
}
