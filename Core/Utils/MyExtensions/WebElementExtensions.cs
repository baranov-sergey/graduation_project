﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using Serilog;

namespace Core.Utils.MyExtensions
{
    public static class WebElementExtensions
    {
        private static object locker = new();
        private static ILogger _log = Logger.Instance;

        public static void ClickAndPageLoad(this IWebElement element, IWebDriver driver, int pageLoadingSeconds = 3)
        {
            lock (locker)
            {
                TimeSpan span = TimeSpan.FromSeconds(pageLoadingSeconds);

                try
                {
                    driver.Manage().Timeouts().PageLoad = span;
                    element.WaitClickableObjectThenClick(driver);
                }
                catch (WebDriverTimeoutException e)
                {
                    _log.Warning(e, $"Page load takes more than {span.TotalSeconds} seconds");
                }

                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
            }
        }

        public static void WaitClickableObjectThenClick(this IWebElement element, IWebDriver driver, int timeoutInSeconds = 30)
        {
            (new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds))).Until(ExpectedConditions.ElementToBeClickable(element));
            element.Click();
        }

        public static void WaitClicableObjectThenClickAndSendKey(this IWebElement element, IWebDriver driver, string text, int seconds = 30)
        {
            element.WaitClickableObjectThenClick(driver);
            element.Clear();
            element.SendKeys(text);
        }

        public static IWebElement WebElementIsVisible(this IWebElement element)
        {
            if (!element.Displayed)
            {
                return null;
            }

            return element;
        }
    }
}
